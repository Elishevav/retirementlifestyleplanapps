//
//  Defaults.swift
//  RetirementLifeStylePlanApps
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019 Meytal Gur. All rights reserved.
//

import UIKit

class Defaults: NSObject {
    
    static var shared = Defaults()
    
    let defaults = UserDefaults.standard
    
    func saveString(str:String!,strKey:String){
        defaults.set(str, forKey: strKey)
        defaults.synchronize()
    }
    
    func saveObject(any:Any?,strKey:String){
        defaults.set(any, forKey: strKey)
        defaults.synchronize()
    }
    
    func loadObject(strKey:String)->Any?{
        let obj : Any! = defaults.object(forKey: strKey as String)
        return obj
    }
    
    func loadStringForKey(strKey:String)->String!{
        let str : String! = defaults.object(forKey: strKey as String) as! String?
        defaults.synchronize()
        
        if (str != nil ){
            print(str)
            return str
        }
        return ""
    }
    
    
    
    func saveBool(bool:Bool!,strKey:String){
        defaults.set(bool, forKey: strKey)
        defaults.synchronize()
    }
    
    func saveInt(value:Int!,strKey:String){
        defaults.set(value, forKey: strKey)
        defaults.synchronize()
    }
    
    func loadInt(strKey:String)->Int{
        let value = defaults.integer(forKey: strKey)
        defaults.synchronize()
        return value
    }
    
    func loadBool(strKey:String)->Bool{
        let bool = defaults.bool(forKey: strKey)
        defaults.synchronize()
        return bool
    }
    
    
    
    
    
    
    
    
    func loadImage(strKey:String)->UIImage?{
        let data : Data? = defaults.object(forKey: strKey as String) as? Data
        
        if data != nil {
            let image = UIImage(data: data!)
            return image
        }
        return nil
    }
    
    func deleteAllKeyAndValue(){
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
    }
    
}


