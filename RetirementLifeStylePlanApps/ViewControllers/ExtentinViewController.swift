//
//  ExtentinViewController.swift
//  RetirementLifeStylePlanApps
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019 Meytal Gur. All rights reserved.
//


import Foundation
import UIKit

extension UIViewController{
    func showAlert(title:String, message:String, handlerOK:((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .cancel, handler: handlerOK)
        alert.addAction(action)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
