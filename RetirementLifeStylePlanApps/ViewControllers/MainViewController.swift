//
//  MainViewController.swift
//  RetirementLifeStylePlanApps
//
//  Created by Elisheva Vakrat on 21/08/2019.
//  Copyright © 2019 Meytal Gur. All rights reserved.
//

import UIKit
//import Firebase
    
    class MainViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
        
        
        var arrayLevels = [LevelDetails]()
        var arrayOfIDs = [String]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            //  FirebaseAuth.signOut()
            initGame()
            
            
        }
        
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if let segueName = segue.identifier {
                
                switch segueName {
                case Strings.Segue_To_VideoVC:
                    let VideosVC  = segue.destination as! VideosViewController
                    VideosVC.levels = sender as? LevelDetails
                    break
                default:
                    break
                }
            }
            
        }
        
        func initGame(){
            
            arrayLevels.append(LevelDetails(isLocked: false, VideoName: "https://player.vimeo.com/video/354812654", backgrounImage: "Video1"))
            arrayLevels.append(LevelDetails(isLocked: false, VideoName: "https://player.vimeo.com/video/354812654", backgrounImage: "Video2"))
            arrayLevels.append(LevelDetails(isLocked: false, VideoName: "https://player.vimeo.com/video/354812654", backgrounImage: "Video3"))
            arrayLevels.append(LevelDetails(isLocked: false, VideoName: "https://player.vimeo.com/video/354812654", backgrounImage: "Video4"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "https://player.vimeo.com/video/354812654", backgrounImage: "Video5"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video6"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video7"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video8"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video9"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video10"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video11"))
            arrayLevels.append(LevelDetails(isLocked: true, VideoName: "video11", backgrounImage: "Video12"))
        
        }
        
        
        
        
        
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrayLevels.count
        }
        
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GameSelectCell
            cell.imgBg.image = UIImage(named: arrayLevels[indexPath.row].backgrounImage)
            if (arrayLevels[indexPath.row].isLocked){
                cell.imgLock.isHidden = false
            }
            else{
                cell.imgLock.isHidden = true
            }
            return cell
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            print("selected \(indexPath.row)")
            if arrayLevels[indexPath.row].isLocked {
                
            }
                //        performSegue(withIdentifier: Strings.Segue_To_Apple_Purchase, sender: nil)
                //   }
            else{
                performSegue(withIdentifier: Strings.Segue_To_VideoVC, sender: arrayLevels[indexPath.row])
            }
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let yourWidth = collectionView.bounds.width/4.0
            let yourHeight = collectionView.bounds.height/3.0
            
            return CGSize(width: yourWidth, height: yourHeight)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets.zero
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
}


